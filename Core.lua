---@diagnostic disable-next-line: undefined-global
if not JimHUD then
    return
end

if string.lower(RequiredScript) == "lib/entry" then

    if not JimHUDCompat then
        JimHUDCompat = {}
        JimHUDCompat.mod_path = ModPath
        JimHUDCompat.compat_lua_path = ModPath .. "lua/"
        JimHUDCompat.save_path = SavePath
        JimHUDCompat.settings_path = JimHUDCompat.save_path .. "JimHUD-compat-warned.json"

        JimHUDCompat.unknown_mods = {}
        JimHUDCompat.incompatible_mods = {}
        JimHUDCompat.conflicting_settings = {}
        JimHUDCompat.compat_hooks = {}
        JimHUDCompat.warned_mods = {}

        JimHUDCompat.KNOWN_MODS = {
            ["SuperBLT"] = { compatible = true },
            ["JimHUD"] = { compatible = true },
            ["JimHUD-Compat"] = { compatible = true },
            ["BLT+"] = { compatible = true },
            ["Simple Mod Updater"] = { compatible = true },

            -- compatible --
            ----------------

            ["Bouncer"] = { compatible = true },
            ["BCCFS Reborn"] = { compatible = true },
            ["BangHUD"] = { compatible = true },
            ["ReLUA"] = { compatible = true },
            ["Punisher"] = { compatible = true },
            ["Error 5"] = { compatible = true },
            ["InfamyRankConverter"] = { compatible = true },

            ["Auto-Fire Sound Fix"] = { compatible = true },
            ["Alphasort Mods"] = { compatible = true },
            ["Anti Voice Spam 2"] = { compatible = true },
            ["Anticrash"] = { compatible = true },
            ["Auto-Discard Parachute"] = { compatible = true },
            ["Auto Weekly-Safe Dropper"] = { compatible = true },
            ["Badass Camera"] = { compatible = true },
            ["Bag Contour"] = { compatible = true },
            ["Blackmarket Persistent Names"] = { compatible = true },
            ["BuilDB"] = { compatible = true },
            ["Bulletstorm-enabled Ammo Bag Contours"] = { compatible = true },
            ["Celer"] = { compatible = true },
            ["Chat Info"] = { compatible = true },
            ["Civilian ! Marker for Drop-ins"] = { compatible = true },
            ["Crewfiles"] = { compatible = true },
            ["Crime Spree Fixes"] = { compatible = true },
            ["Custom Profile Set"] = { compatible = true },
            ["CustomFOV"] = { compatible = true },
            ["Detailed Achievements"] = { compatible = true },
            ["Drag and Drop Inventory"] = { compatible = true },
            ["ECM Low Battery Fix"] = { compatible = true },
            ["ECM time left in chat"] = { compatible = true },
            ["Enhanced Hitmarkers"] = { compatible = true },
            ["Fading Contour"] = { compatible = true },
            ["Filtered Camera Beeps"] = { compatible = true },
            ["Fix Crime Spree Concealment Modifier"] = { compatible = true },
            ["Fixed Mutators"] = { compatible = true },
            ["Flashing Swan Song"] = { compatible = true },
            ["GoonMod's Custom Waypoints"] = { compatible = true },
            ["InspirePriority"] = { compatible = true },
            ["Intimidated Outlines (unsynced)"] = { compatible = true },
            ["Iter"] = { compatible = true },
            ["Kick a friend"] = { compatible = true },
            ["Less Inaccurate Weapon Laser"] = { compatible = true },
            ["Live Long and Prosper"] = { compatible = true },
            ["Lobby Settings"] = { compatible = true },
            ["Marking"] = { compatible = true },
            ["More Weapon Stats"] = { compatible = true },
            ["Mr Dr Fantastic!"] = { compatible = true },
            ["No Armor Color Grading"] = { compatible = true },
            ["No Duplicated Bullets"] = { compatible = true },
            ["No Laser When Aiming Down Sight"] = { compatible = true },
            ["Pager Contour"] = { compatible = true },
            ["PAYDAY 2 Tips Viewer"] = { compatible = true },
            ["Playerdrop"] = { compatible = true },
            ["Prettier Lasers"] = { compatible = true },
            ["QuickKeyboardInput"] = { compatible = true },
            ["Refresh Rate Checker"] = { compatible = true },
            ["Reload Then Run"] = { compatible = true },
            ["Saw Helper"] = { compatible = true },
            ["Search Inventory"] = { compatible = true },
            ["Secret Achievements Status"] = { compatible = true },
            ["Soundeffect When Someone Joins"] = { compatible = true },
            ["Spread Fix"] = { compatible = true },
            ["Switch underbarrel in steelsight"] = { compatible = true },
            ["Two-Click Safe House"] = { compatible = true },
            ["Uppers-enabled First Aid Kit Contours"] = { compatible = true },
            ["Use perk device in vehicle"] = { compatible = true },
            ["The Diamond Puzzle Guide"] = { compatible = true },
            ["Player Hours In Chat"] = { compatible = true },
            ["Peer Mod Display"] = { compatible = true },
            ["XPNotifier"] = { compatible = true },
            ["BeardLib"] = { compatible = true },
            ["Extended Continental Coin Shop Goonmod Standalone"] = { compatible = true },
            ["Map-Based Optimizations"] = { compatible = true },
            ["BLT Networking Fix"] = { compatible = true },
            ["HopLib"] = { compatible = true },
            ["Kill Feed"] = { compatible = true },
            ["Orange KillFeed skulls"] = { compatible = true },
            ["Faster Bullet Impact Queues"] = { compatible = true },
            ["Fixed Ambient Falloff Scale"] = { compatible = true },
            ["Anti Blackscreen"] = { compatible = true },
            ["Extra Heist Info"] = { compatible = true },
            ["Jacket's MiniMap - Modified"] = { compatible = true },
            ["Achievement Alerts"] = { compatible = true },
            ["Achievement Hunter"] = { compatible = true },
            ["Stamina Bar"] = { compatible = true },
            ["Bullet Dismemberment Mod"] = { compatible = true },
            ["Bot Weapons and Equipment"] = { compatible = true },
            ["Better Bots"] = { compatible = true },

            ["Crew Ability: Pied Piper"] = { compatible = true },
            ["Crew Ability: Pockets"] = { compatible = true },
            ["Crew Ability: Spotter"] = { compatible = true },
            ["Keep It Clean!"] = { compatible = true },
            ["Make Technician Great Again!"] = { compatible = true },
            ["Security Camera 2.0"] = { compatible = true },

            ["Sentry Health Display"] = { compatible = true }, -- needs review
            ["Winters Assault Fix"] = { compatible = true }, -- needs review
            ["Yet Another Flashlight"] = { compatible = true }, -- needs review
            ["Moveable Intimidated Cop"] = { compatible = true }, -- needs review
            ["Please, Go There"] = { compatible = true }, -- needs review

            ["Cheater Kicker"] = { compatible = true },
            ["Lobby Inspect"] = { compatible = true },
            ["Newbies go back to overkill"] = { compatible = true },
            ["No Mutants Allowed"] = { compatible = true },

            -- Compat hooks --
            ------------------

            ["Translucent Mission Briefing GUI"] = {
                compatible = true,
                compat_lua = "TranslucentMissionBriefingGUI",
                compat_hooks = {
                    "lib/managers/menu/missionbriefinggui", -- profile selector fix
                },
            },
            ["Lobby Player Info"] = {
                compatible = true,
                compat_lua = "LobbyPlayerInfo",
                compat_hooks = {
                    "lib/managers/menu/contractboxgui", -- move group info box in lobby
                },
            },

            -- Conflicting settings --
            --------------------------

            ["Keepers"] = {
                compatible = true,
                conflicts = {
                    -- floating joker health bar
                    {
                        desc = "Keepers: Show Joker's Health",
                        check = function()
                            ---@diagnostic disable-next-line: undefined-global
                            return Keepers and Keepers.settings and Keepers.settings.show_joker_health
                        end,
                        solutions = {
                            {
                                disable_in_mod = true,
                                run = function()
                                    ---@diagnostic disable-next-line: undefined-global
                                    Keepers.settings.show_joker_health = false
                                    ---@diagnostic disable-next-line: undefined-global
                                    Keepers:save_settings()
                                end,
                            },
                        },
                    },
                },
            },
            ["Full Speed Swarm"] = {
                compatible = true,
                conflicts = {
                    -- optimized inputs / auto pickup
                    {
                        desc = {
                            "Full Speed Swarm: Optimized Inputs",
                            "JimHUD: Gameplay/Controls > Interaction Lock > Auto Pickup"
                        },
                        check = function()
                            ---@diagnostic disable-next-line: undefined-global
                            return FullSpeedSwarm and FullSpeedSwarm.settings and FullSpeedSwarm.settings.optimized_inputs and JimHUD:getSetting({ "INTERACTION", "HOLD2PICK" }, false)
                        end,
                        solutions = {
                            {
                                disable_in_mod = true,
                                run = function()
                                    ---@diagnostic disable-next-line: undefined-global
                                    FullSpeedSwarm.settings.optimized_inputs = false
                                    ---@diagnostic disable-next-line: undefined-global
                                    FullSpeedSwarm:save()
                                end,
                            },
                            {
                                disable_in_jimhud = true,
                                run = function()
                                    ---@diagnostic disable-next-line: undefined-global
                                    JimHUD:setSetting({ "INTERACTION", "HOLD2PICK" }, false)
                                    ---@diagnostic disable-next-line: undefined-global
                                    JimHUD:Save()
                                end,
                            },
                        },
                        disable_in_both = true,
                    },
                },
            },
            ["Monkeepers"] = {
                compatible = true,
                conflicts = {
                    -- disable bot catch
                    {
                        desc = {
                            "Monkeepers: Prevent bots to catch thrown Bags",
                            "JimHUD: Gameplay/Controls > Disable Bot catch"
                        },
                        check = function()
                            ---@diagnostic disable-next-line: undefined-global
                            return Monkeepers and Monkeepers.settings and Monkeepers.settings.disable_catch_thrown_bags and JimHUD:getSetting({ "EQUIPMENT", "DISABLE_BOT_CATCH" }, false)
                        end,
                        solutions = {
                            {
                                disable_in_mod = true,
                                run = function()
                                    ---@diagnostic disable-next-line: undefined-global
                                    Monkeepers.settings.disable_catch_thrown_bags = false
                                    ---@diagnostic disable-next-line: undefined-global
                                    Monkeepers:save_settings()
                                end,
                            },
                            {
                                disable_in_jimhud = true,
                                run = function()
                                    ---@diagnostic disable-next-line: undefined-global
                                    JimHUD:setSetting({ "EQUIPMENT", "DISABLE_BOT_CATCH" }, false)
                                    ---@diagnostic disable-next-line: undefined-global
                                    JimHUD:Save()
                                end,
                            },
                        },
                        disable_in_both = true,
                    },
                },
            },
            ["Better Assault Indicator"] = {
                compatible = true,
                conflicts = {
                    -- disable advanced assault indicator in jimhud
                    {
                        desc = "Better Assault Indicator\n\nJimHUD: Advanced Assault Banner",
                        check = function()
                            ---@diagnostic disable-next-line: undefined-global
                            return BAI and JimHUD:getSetting({ "AssaultBanner", "USE_ADV_ASSAULT" }, false)
                        end,
                        solutions = {
                            {
                                disable_in_jimhud = true,
                                run = function()
                                    ---@diagnostic disable-next-line: undefined-global
                                    JimHUD:setSetting({ "AssaultBanner", "USE_ADV_ASSAULT" }, false)
                                    ---@diagnostic disable-next-line: undefined-global
                                    JimHUD:Save()
                                end,
                            },
                        },
                    },
                },
            },

            -- incompatible --
            ------------------

            -- TODO
            --["Item Hunter"] = {compatible = false}, -- TODO: conflicting with custom objective animation
            --["REQ: Hide Modifiers"] = {compatible = false},
            --["Better Difficulty Names"] = {compatible = false},

            ["Player Shots Pass Through Team AI"] = { compatible = false },
            ["Shots pass through AI"] = { compatible = false },
            ["Create Empty Lobby"] = { compatible = false },
            ["Disable and Decline Telemetry Agreement"] = { compatible = false },
            ["Disabled skin included modifications"] = { compatible = false },
            ["Floating Health Bars"] = { compatible = false },
            ["Instant Cardflip"] = { compatible = false },
            ["Inverted Flashbang Glare"] = { compatible = false },
            ["Lebensauger Sight Fix"] = { compatible = false },
            ["Lobby Player Info Box Tweak"] = { compatible = false },
            ["Loco & Reinfeld Unfix"] = { compatible = false },
            ["Fixed M32 Sights"] = { compatible = false },
            ["Manual Update Check"] = { compatible = false },
            ["NoAds"] = { compatible = false },
            ["No Advertisements"] = { compatible = false },
            ["No Confirm Dialogs"] = { compatible = false },
            ["No Idle Intro"] = { compatible = false },
            ["Enable Numpad Enter For Confirm"] = { compatible = false },
            ["Objective Animation"] = { compatible = false },
            ["Pre U217 HUD"] = { compatible = false },
            ["Reconnect To Server"] = { compatible = false },
            ["Rename Inventory Pages"] = { compatible = false },
            ["Restructured Menus"] = { compatible = false },
            ["Scrollable menus"] = { compatible = false },
            ["Stale Lobby Contract Fix"] = { compatible = false },
            ["Stop the crimespree loss on crash"] = { compatible = false },
            ["disable crimespree reset on crash"] = { compatible = false },
            ["No CrimeSpree Reset on Crash"] = { compatible = false },
            ["Straight To Main Menu"] = { compatible = false },
            ["Translucent Mission Briefing GUI fix"] = { compatible = false },
            ["VanillaHUDPlus"] = { compatible = false },
            ["WolfHUD"] = { compatible = false },
            ["Bulletstorm indicator"] = { compatible = false },
            ["Crimenet Remastered"] = { compatible = false },

            -- broken mods
            --["Anti Intimidated Outlines"] = {compatible = false}, -- fixme?
            --["Bullet Decapitations"] = {compatible = false}, -- broken
            --["Disable Quick Play"] = {compatible = false}, -- broken
            --["Inventory Chat & Player States"] = {compatible = false}, -- broken

            -- obsolete mods
            ["Announcer"] = { compatible = false }, -- obsolete, due to ingame Mod list
            ["Delayed Calls Fix"] = { compatible = false }, -- obsolete, as fixed in SuperBLT
            ["Mod List Lite"] = { compatible = false }, -- obsolete
        }

        -- general log function
        function JimHUDCompat:print_log(...)
            local params = { ... }
            local msg_type, text = table.remove(params, #params), table.remove(params, 1)
            if msg_type then
                if type(text) == "table" or type(text) == "userdata" then
                    local function log_table(userdata)
                        local text = ""
                        for id, data in pairs(userdata) do
                            if type(data) == "table" then
                                log(id .. " = {")
                                log_table(data)
                                log("}")
                            elseif type(data) ~= "function" then
                                log(id .. " = " .. tostring(data) .. "")
                            else
                                log("function " .. id .. "(...)")
                            end
                        end
                    end

                    if not text[1] or type(text[1]) ~= "string" then
                        log(string.format("[JimHUD-Compat] %s:", string.upper(type(msg_type))))
                        log_table(text)
                        return
                    else
                        text = string.format(unpack(text))
                    end
                elseif type(text) == "function" then
                    msg_type = "error"
                    text = "Cannot log function... "
                elseif type(text) == "string" then
                    text = string.format(text, unpack(params or {}))
                end
                text = string.format("[JimHUD-Compat] %s: %s", string.upper(msg_type), text)
                log(text)
            end
        end

        -- Load config
        function JimHUDCompat:Load()
            local corrupted = false
            local file = io.open(self.settings_path, "r")
            if file then
                self.warned_mods = json.decode(file:read("*all"))
                file:close()
            else
                self:print_log("Error while loading, settings file could not be opened (" .. self.settings_path .. ")", "error")
            end
            if corrupted then
                self:Save()
                self:print_log("Settings file appears to be corrupted, resaving...", "error")
            end
        end

        -- Save config
        function JimHUDCompat:Save()
            if table.size(self.warned_mods or {}) > 0 then
                local file = io.open(self.settings_path, "w+")
                if file then
                    file:write(json.encode(self.warned_mods))
                    file:close()
                else
                    self:print_log("Error while saving, settings file could not be opened (" .. self.settings_path .. ")", "error")
                end
            else
                self:print_log("Error while saving, settings table appears to be empty...", "error")
            end
        end

        -- Init compat meta data
        function JimHUDCompat:Init()
            for _, mod in ipairs(BLT.Mods:Mods()) do
                if mod:IsEnabled() then
                    local mod_name = mod.name
                    local data = JimHUDCompat.KNOWN_MODS[mod_name]
                    if data then
                        if data.compatible then
                            -- hook runtime fixes
                            if data.compat_hooks ~= nil and data.compat_lua ~= nil then
                                for _, hook in ipairs(data.compat_hooks) do
                                    JimHUDCompat.compat_hooks[hook] = data.compat_lua
                                end
                            end
                            -- cache conflicting settings
                            if data.conflicts and table.size(data.conflicts) > 0 then
                                for _, conflict in ipairs(data.conflicts) do
                                    conflict.mod = mod.name
                                    table.insert(JimHUDCompat.conflicting_settings, conflict)
                                end
                            end
                        else
                            -- incompatible mods
                            JimHUDCompat.incompatible_mods[mod_name] = mod
                        end
                    else
                        if not JimHUDCompat.warned_mods[mod_name] then -- skip if warned before
                            -- unknown mods
                            JimHUDCompat.unknown_mods[mod_name] = mod
                        end
                    end
                end
            end
        end

        function JimHUDCompat:HasIncompatibleMods()
            return table.size(JimHUDCompat.incompatible_mods) > 0
        end

        function JimHUDCompat:HasConflictingSettings()
            return table.size(JimHUDCompat.conflicting_settings) > 0
        end

        function JimHUDCompat:HasUnknownMods()
            return table.size(JimHUDCompat.unknown_mods) > 0
        end

        function JimHUDCompat:GetConflicts()
            -- filter conflicts
            local result = {}
            for _, conflict in pairs(JimHUDCompat.conflicting_settings) do
                if conflict.check() then
                    table.insert(result, conflict)
                end
            end
            return result
        end

        -- returns true if restart required
        function JimHUDCompat:DisableIncompatibleMods()
            local restart_required = false
            local full_text = managers.localization:text("jimhudcompat_dialog_incompatible_mods_text") .. "\n"
            for name, mod in pairs(JimHUDCompat.incompatible_mods) do
                full_text = full_text .. "\n" .. name
                mod:SetEnabled(false)
                restart_required = true
            end
            QuickMenu:new(
                "[JimHUD-Compat] " .. managers.localization:text("jimhudcompat_dialog_incompatible_mods_title"),
                full_text,
                {
                    { text = managers.localization:text("dialog_ok"), is_cancel_button = true }
                }, true
            )
            return restart_required
        end

        -- returns true if restart still required
        function JimHUDCompat:HandleConflicts(restart_required)
            local conflicts = self:GetConflicts()
            if table.size(conflicts) > 0 then
                JimHUDCompat.conflicts_handled = 0
                local function finalize_callback(conflict_index, handled)
                    JimHUDCompat.conflicts_handled = JimHUDCompat.conflicts_handled + (handled and 1 or 0)
                    if conflict_index == table.size(conflicts) and (restart_required or JimHUDCompat.conflicts_handled > 0) then -- relua on last conflict
                        setup:load_start_menu()
                    end
                end

                for index, conflict in ipairs(conflicts) do
                    local text = managers.localization:text("jimhudcompat_dialog_conflicting_settings_text") .. "\n\n" .. (type(conflict.desc) == "table" and table.concat(conflict.desc, "\n") or conflict.desc)
                    local menu = {}
                    for _, solution in pairs(conflict.solutions) do
                        local desc = solution.desc
                        if solution.disable_in_mod then
                            desc = managers.localization:text("jimhudcompat_button_disable_in", { Mod = conflict.mod })
                        elseif solution.disable_in_jimhud then
                            desc = managers.localization:text("jimhudcompat_button_disable_in", { Mod = "JimHUD" })
                        end
                        table.insert(menu, { text = desc, callback = function()
                            solution.run()
                            finalize_callback(index, true)
                        end })
                    end
                    if conflict.disable_in_both then
                        table.insert(menu, { text = managers.localization:text("jimhudcompat_button_disable_in_both"), callback = function()
                            for _, solution in pairs(conflict.solutions) do
                                solution.run()
                            end
                            finalize_callback(index, true)
                        end })
                    end
                    table.insert(menu, { text = managers.localization:text("dialog_cancel"), callback = function()
                        finalize_callback(index)
                    end })
                    QuickMenu:new(
                        "[JimHUD-Compat] " .. managers.localization:text("jimhudcompat_dialog_conflicting_settings_title"),
                        text, menu, true
                    )
                end

                return false
            end
            return restart_required
        end

        function JimHUDCompat:WarnUnknownMods()
            local full_text = managers.localization:text("jimhudcompat_dialog_unknown_mods_text") .. "\n"
            local warned = false
            for name, _ in pairs(JimHUDCompat.unknown_mods) do
                full_text = full_text .. "\n" .. name
                JimHUDCompat.warned_mods[name] = true
                warned = true
            end
            QuickMenu:new(
                "[JimHUD-Compat] " .. managers.localization:text("jimhudcompat_dialog_unknown_mods_title"),
                full_text,
                { { text = managers.localization:text("dialog_ok"), is_cancel_button = true } },
                true
            )
            if warned then
                JimHUDCompat:Save()
            end
        end

        function JimHUDCompat:OnMainMenu()
            local restart_required = false

            -- disable incompatible mods
            if self:HasIncompatibleMods() then
                restart_required = self:DisableIncompatibleMods()
            end

            if restart_required then
                BLT.Mods:Save() -- save disabled mods
            end

            -- resolve conflicting settings
            if self:HasConflictingSettings() then
                restart_required = self:HandleConflicts(restart_required)
            end

            -- restart if disabled mods but no conflicting settings
            if restart_required then
                setup:load_start_menu() -- relua
            end

            -- warn about unknown mods
            if self:HasUnknownMods() then
                self:WarnUnknownMods()
            end
        end

        -- Load config
        JimHUDCompat:Load()
        -- Init compat meta data
        JimHUDCompat:Init()
    end

    -- Add localization strings
    Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_JimHUDCompat", function(loc)
        local loc_path = JimHUDCompat.mod_path .. "loc/"
        if file.DirectoryExists(loc_path) then
            loc:load_localization_file(string.format("%s/english.json", loc_path), false)
        else
            JimHUDCompat:print_log("Localization folder seems to be missing!", "error")
        end
    end)

    -- Hook main menu
    Hooks:Add("MenuManagerOnOpenMenu", "MenuManagerOnOpenMenu_JimHUDCompat", function(menu_manager, menu_name, position)
        if menu_name == "menu_main" then
            JimHUDCompat:OnMainMenu()
        end
    end)

end

-- Run compat hooks
for hook, lua in pairs(JimHUDCompat.compat_hooks) do
    if string.lower(RequiredScript) == string.lower(hook) then
        dofile(JimHUDCompat.compat_lua_path .. lua .. ".lua")
    end
end
