---@diagnostic disable-next-line: undefined-global
if LPITeamBox and LPITeamBox.Update and ContractBoxGui and ContractBoxGui.update then

	local contractboxgui_update_original = ContractBoxGui.update
	function ContractBoxGui:update(...)
		contractboxgui_update_original(self, ...)

		-- hide lpi team skills title
		local node_gui = managers.menu:active_menu() and managers.menu:active_menu().renderer:active_node_gui()
		---@diagnostic disable-next-line: undefined-global
		if node_gui and alive(LPITeamBox._team_skills_text) then
			---@diagnostic disable-next-line: undefined-global
			LPITeamBox._team_skills_text:set_visible(false)
		end

	end

	---@diagnostic disable-next-line: undefined-global
	local LPITeamBox_Update_original = LPITeamBox.Update
	---@diagnostic disable-next-line: undefined-global
	function LPITeamBox:Update()
		LPITeamBox_Update_original(self)

		-- move lpi team skills panel
		if alive(self._team_skills_panel) and alive(self.contractboxgui._contract_panel) then
			self._team_skills_panel:set_left(self.contractboxgui._panel:w() / 2 - self._team_skills_panel:w() / 2)
			self._team_skills_panel:set_top(0)
		end

	end

end
