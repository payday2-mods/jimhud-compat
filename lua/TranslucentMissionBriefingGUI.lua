local init_original = MissionBriefingGui.init

function MissionBriefingGui:init(...)
	init_original(self, ...)

	if self._multi_profile_item then
		local panel = self._multi_profile_item:panel()
		if alive(panel) and alive(self._panel) then
			panel:set_bottom(self._panel:h())
		end
	end
end
